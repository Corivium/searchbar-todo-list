import React, { Component } from 'react';
import '../node_modules/bulma/bulma.sass';
import { DataFetch } from './Components/DataFetch';
//import { ListFilter } from './Components/ListFilter';
//import { ItemList } from './Components/List';
import { InputField } from './Components/InputField';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      list: []
    }

    // Bind this to functions
    this.addItem = this.addItem.bind(this);
  }


  // Add click event to add items to list
  addItem = (e) => {

    // Prevent button click from submitting form
    e.preventDefault();

    // Create Variables for our list, the item to add and our form
    let list = this.state.list;
    const newItem = document.getElementById('addInput');
    const form = document.getElementById('addItemForm');

    // If our input has a value
    if(newItem.value !== '') {

      // Add item to list
      list.push(newItem.value);
      
      // Then setState new item in form
      this.setState({
        list: list
      });
    
      // Reset the form
      newItem.classList.remove('is-danger');
      form.reset();

    } else {

      // If the input is empty, make the border red because it's required
      newItem.classList.add('is-danger');

    }

  }

  

  render() {
    console.log('List Array Length:', this.state.list.length);
    console.log('State:', this.state);
    return (
      <div className="content">
        <div className="container">
          <section>
            <DataFetch items={this.state.list}/>
          </section>
          <hr />
          <InputField addItem={this.addItem} />
        </div>
      </div>
    );
  }

}



//https://www.iamtimsmith.com/blog/lets-build-a-search-bar-in-react/