import React from 'react';

export const InputField = (props) => {

    return (
        <section className="section">
            <form id="addItemForm" className="form">
                <input type="text" id="addInput" className="input" placeholder="Something that needs to be done ..." />
                <button className="button is-info" onClick={props.addItem} >
                    Add Item
                </button>
            </form>
        </section>
    );

}