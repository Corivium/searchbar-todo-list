import React from 'react';

export class ListFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filtered: []
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            filtered: this.props.items
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            filtered: nextProps.items
        });
    }

    handleChange(e) {
        // hold original version of list
        let currentList = [];

        // hold filtered list before putting into state
        let newList = [];

        // If the searchbar isn't empty
        if (e.target.value !== '') {

            // Assign the original list to currentList
            currentList = this.props.items;

            // Use .filter() to determine which items should be displayed based on search terms
            newList = currentList.filter(item => {

                // Change current item to lowercase
                const lc = item.toLowerCase();

                // Change search term to lowercase
                const filter = e.target.value.toLowerCase();

                // Check to see if the current list item includes the search term
                // If it does, it will be added to newList. Using lowercase eliminates issues with capitals in search terms and content
                return lc.includes(filter);
            });

        } else {

            // If the search bar is empty, set newList to original task list
            newList = this.props.items;
        
        } // end of search check

        // Set the filtered state based on what rules added to newList
        this.setState({
            filtered: newList
        });

    }

    // Add click even to remove/delete items from list
    removeItem = (item) => {

        // Put our list into an array
        const list = this.props.items.slice();

        for(let i = 0; i < list.length; i++) {
        if (item === list[i]) {
            list.splice(i, 1);
        }
        }

        // Set state to list
        this.setState({
            list: list,
        });

    }


    render() {

        console.log('Filtered list', this.state.filtered);
        let key = 0;
        return(
            <div>
                <div className="search-blk-lg">
                    <input type="text" className="input" onChange={this.handleChange} placeholder="Search..." />
                </div>
                <div className="columns is-multiline">
                    {this.state.filtered.map((item =>
                        <div className="column is-one-quarter ">
                            <div className="job-item">
                                { item }&nbsp;
                                <span className="delete" onClick={() => this.removeItem(item)} />
                            </div>
                        </div>    
                    ))}
                </div>
            </div>
        );
    }

}