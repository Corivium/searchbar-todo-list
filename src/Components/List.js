import React from 'react';

export class ItemList extends React.Component {

    render() {

        let key = 0;
        return(
            <section>
                <ul>
                    {this.props.itemList.map(item => (
                        <li key={`Item-${key++}`}>
                            {item} &nbsp;
                            <span className="delete" onClick={() => this.props.removeItem(item)} />
                        </li>
                    ))}
                </ul>
            </section>
        );

        
    }

}