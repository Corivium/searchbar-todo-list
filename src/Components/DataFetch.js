import React from 'react';
import { ListFilter } from './ListFilter';
import config from '../config';

const GSheetAPI = `https://sheets.googleapis.com/v4/spreadsheets/${config.id}/values:batchGet?ranges=Sheet1&majorDimension=ROWS&key=${config.apiKey}`;

export class DataFetch extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            items: []
        };

    }

    componentDidMount() {

        fetch(GSheetAPI)
            .then(results => {
                return results.json();
            }).then(data => {
                let moveList = data.valueRanges[0].values;
                
                moveList.map((move) => {
                    return (
                        <li key={move.results}>
                            {move}&nbsp;
                            <span className="delete" onClick={this.removeItem} />
                        </li>
                    );
                });
                this.setState({ items: moveList });
                console.log('state:', this.state.items);
            });
    }


    render() {

        return(
            <div>
                <ListFilter items={this.state.items} removeItem={this.removeItem} />
            </div>
        );
    }


}